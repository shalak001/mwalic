package walic.libgdxWalic

import com.badlogic.gdx.graphics.Color
import com.badlogic.gdx.graphics.g2d.Batch
import com.badlogic.gdx.scenes.scene2d.*


class TextActor : Actor() {
    var text: String = ""


    override fun draw(batch: Batch, parentAlpha: Float) {
        val color: Color = color
        batch.setColor(color.r, color.g, color.b, color.a * parentAlpha)
        standardFont.draw(batch, text, x, y, width, 1, true)
    }
}


class Button : Actor() {
    var function: (() -> Unit)? = null
    var hoverScale = 1f

    init {
        setSize(defWidth, defHeight)
        originX = defWidth / 2
        originY = defHeight / 2
        addListener(object : InputListener() {
            override fun enter(event: InputEvent?, x: Float, y: Float, pointer: Int, fromActor: Actor?) {
                hoverScale = 1.2f
            }

            override fun exit(event: InputEvent?, x: Float, y: Float, pointer: Int, toActor: Actor?) {
                hoverScale = 1f
            }

            override fun touchDown(event: InputEvent?, x: Float, y: Float, pointer: Int, button: Int): Boolean {
                if (event!!.isHandled)
                    return true
                if (function != null)
                    function!!.invoke()
                return true
            }
        })
    }

    override fun act(delta: Float) {
        setScale(if (function != null) hoverScale else 1f)
    }
    var text = ""
    override fun draw(batch: Batch, parentAlpha: Float) {
        val color: Color = color
        batch.setColor(color.r, color.g, color.b, color.a * parentAlpha)
        batch.draw(
            cardFront, x, y, originX, originY,
            width, height, scaleX, scaleY, rotation
        )
        standardFont.draw(batch, text, x, y + defHeight * 0.7f, defWidth, 1, true)

    }
}