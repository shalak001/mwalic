package walic.libgdxWalic

import com.badlogic.gdx.graphics.g2d.Batch
import com.badlogic.gdx.scenes.scene2d.Actor
import walic.pureEngine.Card

abstract class LibGdxCardsHandeler : Actor() {
    abstract fun refersh()
}

class LeaderActor(private val master: LibGdxRenderer, val defY: Float, val leader: () -> Card?) : LibGdxCardsHandeler() {
    init {
        master.stage.addActor(this)
    }

    override fun refersh() {
        master.getActorOrCreate(leader() ?: return).apply {
            setPosWithAniamtion(leaderX, defY)
            controller = this@LeaderActor
        }
    }
}

//class EnemyDeck(private val master: LibGdxRenderer, val defY: Float, val cards: MutableList<CardActor>) : LibGdxCardsHandeler() {
//    init {
//        width = defWidth
//        height = defHeight
//        master.stage.addActor(this)
//        this.x = midHidden
//        this.y = defY
//    }
//
//    override fun draw(batch: Batch, parentAlpha: Float) {
//        super.draw(batch, parentAlpha)
//        batch.setColor(color.r, color.g, color.b, color.a * parentAlpha)
//        batch.draw(cardFront, x, y, originX, originY, width, height, scaleX, scaleY, rotation)
//        standardFont.draw(batch, "Enemy Deck", x - defWidth, y + defHeight * 0.7f, defWidth, 1, true)
//    }
//
//    override fun refersh() {
//        cards.toMutableList().forEach {
//            if (it.controller==null) {
//                it.controller = this
//                it.setPosWithAniamtion(x, y)
//            }else{
//                cards.remove(it)
//            }
//        }
//        this.zIndex = Int.MAX_VALUE
//    }
//}