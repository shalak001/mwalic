package walic.libgdxWalic

import com.badlogic.gdx.graphics.Color
import com.badlogic.gdx.graphics.g2d.Batch
import com.badlogic.gdx.math.Interpolation
import com.badlogic.gdx.math.Vector2
import com.badlogic.gdx.scenes.scene2d.Actor
import com.badlogic.gdx.scenes.scene2d.Stage

class Bullet(val start: Actor, val end: Actor, color: Color, stage: Stage, var timeScale: Float = 1.5f) : Actor() {
    init {
        this.color = color
        stage.addActor(this)
        setSize(defWidth / 3, defHeight / 3)
        if (start == end) {
            timeScale = 0.05f
        }
    }


    var time = 0f
    val interpol = Interpolation.pow2
    override fun act(delta: Float) {
        val start = Vector2(start.x+start.width/2, start.y+start.height/2) //start.localToStageCoordinates(Vector2(start.originX, start.originY))
        val finish =Vector2(end.x+end.width/2, end.y+end.height/2)//end.localToStageCoordinates(Vector2(end.originX, end.originY))
        time += delta
        if (time > timeScale) {
            remove()
        }
        x = interpol.apply(start.x, finish.x, time / timeScale)
        y = interpol.apply(start.y, finish.y, time / timeScale)
    }

    override fun draw(batch: Batch, parentAlpha: Float) {
        batch.draw(
            cardFront, x, y, originX, originY,
            width, height, scaleX, scaleY, rotation
        )
    }


}