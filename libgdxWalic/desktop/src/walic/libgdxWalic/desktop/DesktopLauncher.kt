package walic.libgdxWalic.desktop


import com.badlogic.gdx.backends.lwjgl3.Lwjgl3Application
import com.badlogic.gdx.backends.lwjgl3.Lwjgl3ApplicationConfiguration
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking
import walic.gwentBeta.initGwentBeta
import walic.libgdxWalic.InitialRenderer
import walic.pureEngine.RadnomAdapter
import walic.pureEngine.State
import walic.pureEngine.SteamAdapter
import kotlin.random.Random


fun playReplay(replayPath: String?) {
//    val file = File(if (replayPath != null) replayPath else {
//        println("Enter filename");"replies/" + readLine()!!
//    })
//    val replay = creatReplyFromString(file.readText())
//    val adapter = replay.getStreamAdapter()
//    State(adapter, false).mainLoop()
}

suspend fun runTest() {
    (0 until 6).map { tId ->
        GlobalScope.launch {
            while (true) {
                val streams = listOf(Channel<String>(10), Channel(10))
                listOf(0, 1).map { p ->
                    launch {
                        (0..Int.MAX_VALUE).forEach { inter ->
                            val randomSeed = Random.nextLong()
                            val adapter = RadnomAdapter(true, Random(randomSeed))
                            val steamAdapter = SteamAdapter(streams[p]::receive, streams[1 - p]::send, p, adapter, 0)
                            try {
                                State(steamAdapter).mainLoop()
                            } catch (e: Exception) {
                                println("Random seeed was $randomSeed for $p")
                                adapter.log.forEach { println("$p -> $it") }
//                                userHomeVfs["replies/crushDump$p-$inter.wrp"].write(
//                                    steamAdapter.getReplay().toByteArray()
//                                )
                                streams[1 - p].send("Some random value that will crash that other stream Xd no the best idea but whatever")
                                streams[1 - p].send("}[} one more time to be sure")
                                throw e
                            }

                            if (p == 0 && tId == 0)
                                println("Iter $inter")

                        }
                    }
                }.forEach { it.join() }
            }
        }
    }.forEach { it.join() }
}

object DesktopLauncher {


    @JvmStatic
    fun main(args: Array<String>) {
        if (args.firstOrNull() == "test") {
            runBlocking {
                initGwentBeta()
                runTest()
            }
            return
        }
        val hostname = args.toList().elementAtOrElse(0) { "auto" }
        val config = Lwjgl3ApplicationConfiguration()
        config.setWindowedMode(1920, 1080)
        Lwjgl3Application(InitialRenderer(hostname), config)
    }
}





