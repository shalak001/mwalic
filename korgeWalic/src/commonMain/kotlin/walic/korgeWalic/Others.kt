package walic.korgeWalic

import com.soywiz.klock.seconds
import com.soywiz.korge.input.onClick
import com.soywiz.korge.input.onOut
import com.soywiz.korge.input.onOver
import com.soywiz.korge.tween.duration
import com.soywiz.korge.tween.easing
import com.soywiz.korge.tween.get
import com.soywiz.korge.tween.tween
import com.soywiz.korge.view.*
import com.soywiz.korim.color.Colors
import com.soywiz.korio.async.launchImmediately
import com.soywiz.korma.geom.vector.circle
import com.soywiz.korma.interpolation.Easing
import kotlinx.coroutines.Job
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.sync.Mutex


abstract class CardGroupAdapter : Container()

class NiceRect(val master: RubeGoldbergRenderer) : Container() {
    init {
        master.stage.addChild(this)
        image(backBitmap) {
            width = xSep
            height = ySep
            x = -xSep / 2
            y = -ySep / 2
        }
    }

    var mover: Job? = null
    private val defMovemntTime = 1.0.seconds
    fun setNewPos(newX: Double, newY: Double, newWith: Double, newHeight: Double, time: Double) {
        mover?.cancel()
        mover = master.stage.launchImmediately {
            tween(
                    this::x[newX].easing(Easing.EASE_IN_OUT_QUAD),
                    this::y[newY].easing(Easing.EASE_IN_OUT_QUAD),
                    this::scaleX[newWith].easing(Easing.EASE_IN_OUT_QUAD),
                    this::scaleY[newHeight].easing(Easing.EASE_IN_OUT_QUAD),
                    time = defMovemntTime * time
            )
        }
    }
}

class OkButton(val master: RubeGoldbergRenderer) : Container() {

//    fun dalekoJeszcze():Boolean
//    {
//        return mutex!=null
//    }
    val pickedCards = mutableListOf<Int>()
    var canLess: Boolean = false
    var cardsToPick: Int = 0
    var mutex: Channel<List<Int>>? = null
    var hoverScale = 1f

    init {
        master.stage.addChild(this)
    }

    private val im = image(frontBitmap) {
        width = defWidth
        height = defHeight
        x = -defWidth / 2
        y = -defHeight / 2
    }
    private val tx = text("", 24.0, Colors.WHITE, font)

    init {
        onOver {
            hoverScale = 1.2f
        }
        onOut {
            hoverScale = 1f
        }
        onClick {
            println("Click $pickedCards")
            if (isAllOk()) {
                mutex?.send(pickedCards)
//                mutex=null
            }
        }
        addUpdater {
            val pN = pickedCards.size
            if (pN == cardsToPick && pN == 1)
                master.stage.launchImmediately {
                    mutex?.send(pickedCards)
//                    mutex=null
                }
            tx.text = if (mutex != null) {
                if (canLess && cardsToPick == 1) {
                    "Skip"
                } else {
                    if (canLess)
                        "$pN/<=/$cardsToPick"
                    else
                        "$pN/=/$cardsToPick"
                }
            } else ""
            tx.centerOn(im)
            scale = if (mutex != null && isAllOk()) master.osciScale * hoverScale else 1.0
        }

    }

    private fun isAllOk(): Boolean {
        return pickedCards.size == cardsToPick || (pickedCards.size < cardsToPick && canLess)

    }


}


class Bullet(val firerer: CardActor, val target: CardActor, master: RubeGoldbergRenderer, var timeScale: Float = 1f) : Container() {
    var t = 0.0

    init {
        image(frontBitmap) {
            width = defWidth / 3
            height = defHeight / 3
            x = -defWidth / 6
            y = -defHeight / 6
        }
        master.stage.addChild(this)
        if (firerer == target) {
            timeScale = 0.05f
        }
        master.stage.launchImmediately {
            master.stage.tween(this::t[0.0, 1.0].duration(1.seconds).easing(Easing.EASE_IN_OUT_QUAD))
            master.stage.removeChild(this)
        }

        master.stage.addUpdater {
            this@Bullet.pos = firerer.pos * (1 - t) + target.pos * t
        }


    }

    fun getLongTime(): Long {
        return (1000L * timeScale).toLong()
    }

}