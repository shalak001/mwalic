package walic.korgeWalic

import com.soywiz.klock.DateTime
import com.soywiz.klock.milliseconds
import com.soywiz.korio.async.delay
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.sync.Mutex
import walic.pureEngine.*
import kotlin.random.Random


class GetLater<A> {
    private var a: A? = null
    private val mutex = Mutex()
    suspend fun lock(): GetLater<A> {
        mutex.lock()
        return this
    }

    fun set(a: A) {
        this.a = a
        mutex.unlock()
    }

    suspend fun get(f: suspend () -> Unit): A {
        mutex.lock()
        val aret = a!!
        f()
        return aret

    }
}


fun getMiliseconds(): Long {
    return DateTime.now().unixMillis.toLong()
}

class GuiAdapter(val rubeGoldbergRenderer: RubeGoldbergRenderer, val deck: List<String>?) :
    PlayerAdapter() {    //graphics

    lateinit var state: State
    lateinit var sih: SecretInformationHandler
    var playerId: Int = -1

    override fun setRequierd(state: State, noDeck: Boolean): Pair<List<String>, Long> {

        val random = Random.Default
        this.state = state
        this.sih = state.steamAdapter.secretInformationHandler
        this.playerId = state.steamAdapter.playerNumber
        rubeGoldbergRenderer.setRequierd(state, playerId, sih)
        return Pair(
            when {
                noDeck -> listOf()
                deck != null -> deck
                else -> {
                    val leader = allLeaders.random(random)
                    val fraction = leader.fraction!!
                    val possible = allNormalCards.filter { it.isFraction(fraction) }
                    val deck = mutableListOf(leader)
                    possible.filter { it.ist(Tag.Gold) }.pickUpToN(4, random).forEach { deck.add(it) }
                    possible.filter { it.ist(Tag.Silver) }.pickUpToN(6, random).forEach { deck.add(it) }
                    possible.filter { it.ist(Tag.Bronze) }.pickUpToN(5, random)
                        .forEach { card -> repeat(3) { deck.add(card) } }
                    deck.map { it::class.simpleName!! }
                }
            }, random.nextLong())
    }


    override suspend fun pickRow(mask: RowMask): CardLocation {
        val ret = GetLater<CardLocation>().lock()
        rubeGoldbergRenderer.appluFunction {
            rubeGoldbergRenderer.lines.filter { mask.check(playerId, it.location) }.forEach { it.laterGetter = ret }
        }
        return ret.get {
            rubeGoldbergRenderer.appluFunction {
                rubeGoldbergRenderer.lines.forEach { it.laterGetter = null }
            }
        }
    }

    override suspend fun pickPlaceBetweenCards(card: Card, mask: RowMask): CardLocation {
        val ret = GetLater<CardLocation>().lock()
        rubeGoldbergRenderer.appluFunction {
            rubeGoldbergRenderer.mouseFollower = rubeGoldbergRenderer.getActor(card)
            rubeGoldbergRenderer.lines.filter { mask.check(playerId, it.location) }.forEach { it.laterGetter = ret }
        }
        return ret.get {
            rubeGoldbergRenderer.appluFunction {
                rubeGoldbergRenderer.lines.forEach { it.laterGetter = null }
                rubeGoldbergRenderer.mouseFollower = null
            }
        }
    }

    class ButtonCard(override val description: String) : AbstractCard() {
        override val defaultStrenght = 0
        override val tags = listOf<Tag>()

    }

    override suspend fun <A> pickFromList(list: List<A>, n: Int, canLess: Boolean): List<Int> {

        val chanell = Channel<List<Int>>(Channel.CONFLATED)
        rubeGoldbergRenderer.appluFunction { }
        if (!canLess && list.size == n) {
            return list.indices.toList()
        }
        rubeGoldbergRenderer.appluFunction {
            list.map { a: A ->
                if (a is Card) {
                    rubeGoldbergRenderer.getActor(a)
                } else {
                    val card = if (a is AbstractCard)
                        Card(a, 0, null)
                    else
                        Card(ButtonCard(a.toString()), 0, null)
                    val locActor = CardActor(rubeGoldbergRenderer, card)
                    rubeGoldbergRenderer.otherCardsList.add(locActor)
                    locActor
                }
            }.forEachIndexed { index, cardActor ->
                cardActor.pickableIndex = index
            }
            rubeGoldbergRenderer.okOkButton.cardsToPick = n
            rubeGoldbergRenderer.okOkButton.canLess = canLess
            rubeGoldbergRenderer.okOkButton.mutex = chanell
        }
        println("Reached ")
        val ret = chanell.receive().toMutableList()
        rubeGoldbergRenderer.appluFunction {
            rubeGoldbergRenderer.otherCardsList.forEach { it.removeFromParent() }
            rubeGoldbergRenderer.otherCardsList.removeAll { true }
            rubeGoldbergRenderer.okOkButton.pickedCards.removeAll { true }
            rubeGoldbergRenderer.okOkButton.mutex = null
            rubeGoldbergRenderer.mapCards.values.forEach { actor ->
                actor.pickableIndex = null
            }
            rubeGoldbergRenderer.openLine?.switchState()
        }
        return ret
    }


    private var lastMessage: State.PrintOption? = null
    private var lastCard: Card? = null

    private var timeToSleepTo = getMiliseconds()
    override suspend fun printMess(printOption: State.PrintOption, card: Card?) {

        val lastMessage = lastMessage
        val lastCard = lastCard


        if (printOption !is State.PrintOption.InfoForPlayer)
            println("$printOption by $card")
        val lastMessageKotlin = lastMessage

        if (printOption is State.PrintOption.Simple && lastMessageKotlin is State.PrintOption.Simple)
            if (printOption.mod == lastMessageKotlin.mod && printOption.other != lastMessageKotlin.other)
                timeToSleepTo = getMiliseconds()

        if (printOption is State.PrintOption.WithValue && lastMessageKotlin is State.PrintOption.WithValue) {
            if (printOption.value == lastMessageKotlin.value && printOption.other != lastMessageKotlin.other && printOption.mod == lastMessageKotlin.mod)
                timeToSleepTo = getMiliseconds()
        }

        val sleepTime = timeToSleepTo - getMiliseconds()
        if (sleepTime > 0)
            delay(sleepTime.milliseconds)
        timeToSleepTo = getMiliseconds()

        rubeGoldbergRenderer.appluFunction {
            when (printOption) {
                is State.PrintOption.Simple -> {
                    when (printOption.mod) {
                        State.PrintOption.SingleMods.Healed -> {
                        }
                        State.PrintOption.SingleMods.Resurected -> {
                        }
                        State.PrintOption.SingleMods.Spawned -> {
                            if (card != null)
                                rubeGoldbergRenderer.spawnCardOnSide(printOption.other, card.owner)
                        }
                        State.PrintOption.SingleMods.LockToggled -> {
                        }
                        State.PrintOption.SingleMods.Moved -> {
                        }
                        State.PrintOption.SingleMods.ArmourRemoved -> {
                        }
                        State.PrintOption.SingleMods.Resetet -> {
                        }
                        State.PrintOption.SingleMods.Destroyed -> {
                        }
                        State.PrintOption.SingleMods.Charmed -> {
                        }
                        State.PrintOption.SingleMods.Resilance -> {
                        }
                        State.PrintOption.SingleMods.Created -> {
                        }
                        State.PrintOption.SingleMods.ReturedToHand -> {
                        }
//                        State.PrintOption.SingleMods.ReturnToDeck -> {
//                        }
                        State.PrintOption.SingleMods.Summoned -> {
                            if (card != null)
                                rubeGoldbergRenderer.spawnCardOnSide(printOption.other, card.owner)
                        }
                        State.PrintOption.SingleMods.ActionApplyedToReveldCard -> {
                        }
                        State.PrintOption.SingleMods.BoostedInHand -> {
                        }
//                        State.PrintOption.SingleMods.RemovedFromHand -> {
//                            if (!sih.cardToDraw.map { it.uuid }
//                                    .contains(rubeGoldbergRenderer.getActor(printOption.other).card.uuid))
//                                rubeGoldbergRenderer.enemyDeckCards.add(printOption.other)
//                        }
                        State.PrintOption.SingleMods.Discarded -> {
                        }
                        State.PrintOption.SingleMods.Banished -> {
                        }
                        State.PrintOption.SingleMods.PlayedFromHand -> {
                        }
                    }
                    var show = card != null
                    if (printOption.mod == State.PrintOption.SingleMods.Destroyed && lastMessage != null) {
                        if (lastMessage is State.PrintOption.Simple)
                            if (lastMessage.other == printOption.other && lastCard == card)
                                show = false
                        if (lastMessage is State.PrintOption.WithValue)
                            if (lastMessage.other == printOption.other && lastCard == card)
                                show = false
                    }
                    if (show) {
                        timeToSleepTo += Bullet(
                            rubeGoldbergRenderer.getActor(card!!),
                            rubeGoldbergRenderer.getActor(printOption.other),
                            rubeGoldbergRenderer
                        ).getLongTime()
                    }
                }
                is State.PrintOption.WithValue -> {
                    if (card != null) {
                        timeToSleepTo += Bullet(
                            rubeGoldbergRenderer.getActor(card),
                            rubeGoldbergRenderer.getActor(printOption.other),
                            rubeGoldbergRenderer
                        ).getLongTime()

                    }
                }
                is State.PrintOption.Draw -> {
                    printOption.drawn.forEach {
                        rubeGoldbergRenderer.spawnCardOnSide(it, printOption.who)
                    }
                }
                is State.PrintOption.Reveled -> {
                    rubeGoldbergRenderer.mapCards[printOption.final.uuid] =
                        rubeGoldbergRenderer.getActor(printOption.initial)
                    timeToSleepTo += rubeGoldbergRenderer.getActor(printOption.initial).reveal(printOption.final)
                        .toLong()
                }
                is State.PrintOption.Other -> {
                }
                is State.PrintOption.Dueal -> {
                }
                is State.PrintOption.Drain -> {
                }
                is State.PrintOption.Simpler -> {
                    when (printOption.mod) {
                        State.PrintOption.SimplerMods.ActionOnDeck -> {
                        }
                        State.PrintOption.SimplerMods.GetInfoAboutInitialDeck -> {
                        }
                        State.PrintOption.SimplerMods.BoostRandomInHand -> {
                        }
                        State.PrintOption.SimplerMods.BoostAllInDeckAndHand -> {
                        }
                        State.PrintOption.SimplerMods.ApplyActionAndMoveToTop -> {
                        }
                        State.PrintOption.SimplerMods.InHandValue -> {
                        }
                        State.PrintOption.SimplerMods.CreateCardFromEnemyStartingDeck -> {
                        }
                        State.PrintOption.SimplerMods.GetInfoAboutHand -> {

                        }
                    }
                }
                is State.PrintOption.ApplyWether -> {
                }
                is State.PrintOption.Transform -> {
                    //TODO
                }
                is State.PrintOption.InfoForPlayer -> {
                }
            }

            var text = printOption.toString() + if (card != null) " by $card" else ""

            if (printOption is State.PrintOption.Reveled && !printOption.shouldBePrinted) {
                return@appluFunction
            }

            if (lastMessage is State.PrintOption.InfoForPlayer) {
                rubeGoldbergRenderer.messageText.last().text = text
            } else {
                rubeGoldbergRenderer.messageText.indices.reversed().forEach {
                    val tempText = rubeGoldbergRenderer.messageText[it].text
                    rubeGoldbergRenderer.messageText[it].text = text
                    text = tempText
                }
            }
            this.lastCard = card
            this.lastMessage = printOption
        }
    }
}