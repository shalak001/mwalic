package walic.pureEngine


//todo ambushes
//todo maks per row

const val turnsToWin = 2


data class RowMask(val ally: List<Boolean>, val enemy: List<Boolean>) {
    fun check(playerId: Int, whoseLane: Int, laneId: Int): Boolean {
        return (if (playerId == whoseLane) ally else enemy)[laneId]
    }

    fun check(playerId: Int, cardLocation: CardLocation): Boolean {
        return (if (playerId == cardLocation.playerId) ally else enemy)[cardLocation.rowId]
    }

    operator fun plus(rowMask: RowMask): RowMask {
        val ally = this.ally.mapIndexed { index: Int, b: Boolean -> rowMask.ally[index] || b }
        val enemy = this.enemy.mapIndexed { index: Int, b: Boolean -> rowMask.enemy[index] || b }
        return RowMask(ally, enemy)
    }

    operator fun minus(rowMask: RowMask): RowMask {
        val ally = this.ally.mapIndexed { index: Int, b: Boolean -> !rowMask.ally[index] && b }
        val enemy = this.enemy.mapIndexed { index: Int, b: Boolean -> !rowMask.enemy[index] && b }
        return RowMask(ally, enemy)
    }

    operator fun times(rowMask: RowMask): RowMask {
        val ally = this.ally.mapIndexed { index: Int, b: Boolean -> rowMask.ally[index] && b }
        val enemy = this.enemy.mapIndexed { index: Int, b: Boolean -> rowMask.enemy[index] && b }
        return RowMask(ally, enemy)
    }

}

val mAny = RowMask(listOf(true, true, true), listOf(true, true, true))
val mAlly = RowMask(listOf(true, true, true), listOf(false, false, false))
val mEnemy = RowMask(listOf(false, false, false), listOf(true, true, true))

fun maskOnlyRow(playerId: Int, location: CardLocation): RowMask {
    val sec = mutableListOf(false, false, false)
    sec[location.rowId] = true
    return if (playerId == location.playerId)
        RowMask(enemy = listOf(false, false, false), ally = sec)
    else
        RowMask(ally = listOf(false, false, false), enemy = sec)
}


class State(val steamAdapter: SteamAdapter, private val veryfieAfter: Boolean = true) {
    val players = List(2) { Player(this) }
    var cardPlayedHistory = mutableListOf<Pair<AbstractCard, Int>>()
    suspend fun mainLoop() {
        var roundNumber = 0
        var whoesTrun: Int

        players.indices.forEach { pi ->
            players[pi].leader = steamAdapter.getInfoWithUserInput<Pair<List<String>, Long>, Card>(pi) { eval, sih ->
                sih.loadCards(eval { it.setRequierd(this) })
            }
            players[pi].fraction = players[pi].leader?.fraction!!
        }
        try {
            players.indices.forEach { p ->
                draw(p, 10)
            }

            whoesTrun = getRng().nextInt(0, 2)
            players.indices.forEach { muligan(it, ammount = 2) }

            while (true) {
                players.map { it.rows zip it.weathers }.flatten().forEach { (row, wether) -> wether?.onTurn(this, row.toMutableList(), getRng()) }


                callForAll("onTurnStart", whoesTrun) { it.onTurnStart() }
                players[whoesTrun].graveyard.toMutableList().forEach { card -> callForSingle("onTurnStartInGraviard", card) { it.onTurnStartInGraviard() } }

                if (players[whoesTrun].didPass)
                    customPrint("Player $whoesTrun did alredy pass")
                else
                    if (!forceToPlay(whoesTrun, canLeaderAndPass = true)) {
                        customPrint("Player $whoesTrun passes")
                        players[whoesTrun].didPass = true
                    }
                if (players.all { it.didPass }) {
                    val s = players.map { it.getStrenght() }
                    if (s[0] == s[1]) {
                        customPrint("Tie")
                        players.forEach { it.wins += 1 }
                        whoesTrun = getRng().nextInt(0, 2)
                    } else {
                        val winner = if (s[0] > s[1]) 0 else 1
                        customPrint("Winner of round is $winner")
                        players[winner].wins += 1
                        whoesTrun = winner
                    }

                    if (players[0].wins >= turnsToWin && players[1].wins >= turnsToWin) {
                        setWinner(-1)
                        return
                    }
                    for (p in players.indices) {
                        if (players[p].wins >= turnsToWin) {
                            setWinner(p)
                            return
                        }
                    }
                    //cleanup
                    getAlCardsOnBattleFiled().toMutableList().forEach {
                        if (!it.resilience)
                            it.destroy(true)
                        else {
                            it.reset()
                            it.resilience = false
                        }
                    }
                    players.indices.forEach { p -> (0..2).forEach { applyWeather(CardLocation(p, it), null) } }
                    players.forEach { player ->
                        player.graveyard.toMutableList().forEach { card ->
                            callForSingle(
                                "onStartOfRoundInGraviard",
                                card
                            ) { it.onStartOfRoundInGraviard() }
                        }

                    }
                    players.forEach { it.didPass = false }
                    roundNumber += 1
                    when (roundNumber) {
                        1 -> {
                            players.indices.forEach { draw(it, 2);muligan(it, ammount = 2) }
                        }
                        2 -> {
                            players.indices.forEach { draw(it, 1);muligan(it, ammount = 1) }
                        }
                        else -> throw WrongInputException("Wrong round numer")
                    }

                } else {
                    whoesTrun = 1 - whoesTrun
                }
                callForAll("onTurnsEnd", whoesTrun) { it.onTurnsEnd() }
                players[whoesTrun].graveyard.toMutableList()
                    .forEach { card -> callForSingle("onTurnsEndInGraviard", card) { it.onTurnsEndInGraviard() } }


            }
        }catch (e:Exception)        {
            customPrint(e.toString())
        }
    }

    private suspend fun customPrint(string: String) {
        customPrint(PrintOption.Other(string))
    }


    sealed class PrintOption {
        enum class SingleMods {
            Healed,
            Resurected,
            Spawned,
            LockToggled,
            PlayedFromHand,
            Moved,
            ArmourRemoved,
            Resetet,
            Destroyed,
            Charmed,
            Resilance,
            Created,
            ReturedToHand,
            Summoned,
            ActionApplyedToReveldCard,
            BoostedInHand,
            Discarded,
            Banished
        }

        enum class ModsWithValue {
            Damaged,
            Boosted,
            Strenghten,
            Weeken,
            Armored,
            CurrentSrenghtSettet,
            BaseSrenghtSettet,

        }

        enum class SimplerMods {
            ActionOnDeck,
            GetInfoAboutInitialDeck,
            BoostRandomInHand,
            BoostAllInDeckAndHand,
            ApplyActionAndMoveToTop,
            InHandValue,
            CreateCardFromEnemyStartingDeck,
            GetInfoAboutHand,
        }

        open class WithTarget(val other: Card) : PrintOption()

        class Simple(val mod: SingleMods, other: Card) : WithTarget(other) {
            override fun toString(): String {
                return "Card $other gets ${mod.toString().camelToHumanRedable()} "
            }
        }

        class WithValue(val mod: ModsWithValue, other: Card, val value: Int) : WithTarget(other) {
            override fun toString(): String {
                val spujnik = when (mod) {
                    ModsWithValue.CurrentSrenghtSettet, ModsWithValue.BaseSrenghtSettet -> "to"
                    else -> "by"
                }
                return "Card $other gets ${mod.toString().camelToHumanRedable()}  $spujnik $value"
            }
        }

        data class Simpler(val mod: SimplerMods) : PrintOption() {
            override fun toString(): String {
                return "The folloing happens \"${mod.toString().camelToHumanRedable()}\""
            }
        }

        data class ApplyWether(val row: CardLocation, val weather: Weather?) : PrintOption() {
            override fun toString(): String {
                return if (weather == null) {
                    "Wether gets removed from row ${row.rowId} of player ${row.playerId}"
                } else {
                    "Wether gets sets to ${
                        Weather::class.simpleName.toString().camelToHumanRedable()
                    } on row ${row.rowId} of player ${row.playerId}"
                }
            }
        }

        data class Draw(val who: Int, val drawn: List<Card>, val isSwap: Boolean = false) : PrintOption() {
            override fun toString(): String {
                val czasownik = if (isSwap) "swaps " else "draws"
                val notNull = drawn.filter { it.ac !is UnknowCard }.map { it.toString() }
                if (drawn.size == 1 && notNull.size == 1)
                    return "Player $who draw ${notNull[0]}"
                return "Player $who $czasownik ${drawn.size} cards" + if (notNull.isNotEmpty()) "indlucing " + notNull.joinToString(
                    ","
                ) else ""
            }
        }

        data class Reveled(val initial: Card, val final: Card, val shouldBePrinted: Boolean = true) : PrintOption() {
            override fun toString(): String {
                return "Card $initial  is now $final"
            }
        }

        data class Transform(val from: AbstractCard, val to: Card) : PrintOption()

        data class Dueal(val first: Card, val secound: Card) : PrintOption()
        data class Drain(val target: Card, val reciver: Card) : PrintOption()

        data class Other(val message: String) : PrintOption() {
            override fun toString(): String {
                return message
            }
        }

        data class InfoForPlayer(val message: String) : PrintOption() {
            override fun toString(): String {
                return "Info: $message"
            }
        }

        data class AddToDeck(val card: Card, val playerId: Int) : PrintOption() {
            override fun toString(): String {
                return "Card $card gets added to  hand of player $playerId"
            }
        }
    }

    suspend fun customPrint(type: PrintOption) {
        steamAdapter.playerAdapter.printMess(type, currentActingCard.elementAtOrNull(0))
    }


    var currentActingCard = mutableListOf<Card>()


    private suspend fun setWinner(winner: Int) {
        //whatever
        customPrint("Winner is $winner")
        if (veryfieAfter)
            steamAdapter.verify()
    }


    private suspend fun muligan(playerId: Int, ammount: Int) {
        swap(playerId, ammount = ammount)
    }

    fun getAlCardsOnBattleFiled(playerId: Int = -1): List<Card> {
        return players.filterIndexed { p, _ -> p != 1 - playerId }.map { it.rows }.flatten().flatten()
    }

    fun getAlCardsOnBattleFiled(playerId: Int, mask: RowMask): List<Card> {
        val ofTheYedi = mutableListOf<Card>()
        for (p in players.indices)
            for (l in players[p].rows.indices) {
                if (mask.check(playerId, p, l))
                    ofTheYedi += players[p].rows[l]
            }
        return ofTheYedi
    }


    class Player(state: State) { //hold information that is per player
        val rows = List(3) { mutableListOf<Card>() }
        val weathers = mutableListOf<Weather?>(null, null, null)
        val graveyard = mutableListOf<Card>()
        val banished = mutableListOf<Card>()
        val spawners = allSpawner.map { Card(it, -1L, state) }

        val knownHand = mutableListOf<Card>()

        lateinit var fraction: Tag
        var leader: Card? = null


        var wins = 0
        var didPass = false

        fun getStrenght(): Int {
            return rows.sumBy { l -> l.sumBy { it.currentStrength } }
        }

    }

    fun getCardByLocation(cardLocation: CardLocation): Card? {
        return players[cardLocation.playerId].rows[cardLocation.rowId].elementAtOrNull(cardLocation.positionId)
    }

    fun getRowByLocation(cardLocation: CardLocation): MutableList<Card> {
        return players[cardLocation.playerId].rows[cardLocation.rowId]
    }

    internal fun putCard(card: Card, cardLocation: CardLocation) {
        getRowByLocation(cardLocation).add(cardLocation.positionId, card)
    }


    //get function, random shit goes here
    fun getRandomCardOnBattlefiled(
        playerId: Int,
        mask: RowMask = mEnemy,
        function: (Card) -> Boolean = { true }
    ): Card? {
        val cards = getAlCardsOnBattleFiled(playerId, mask).filter(function)
        return cards.randomOrNull(getRng())

    }

    fun getRng(): kotlin.random.Random {
        return steamAdapter.random
    }

    inline fun callForAll(whatToPrint: String, playerId: Int = -1, function: (Card) -> Unit) {
        getAlCardsOnBattleFiled(playerId).forEach { if (!it.notOnBattlefiled()) callForSingle(whatToPrint, it, function) }
        players.map { it.spawners }.flatten().forEach { callForSingle(whatToPrint, it, function) }
    }

    inline fun callForSingle(whatToPrint: String, card: Card, function: (Card) -> Unit) {
        if (!card.locked) {
            currentActingCard.add(0, card)
            function(card)
            currentActingCard.removeAt(0)
        }
        try {
            steamAdapter.checkIfMakesSens(this)
        } catch (e: Exception) {
            println("Winna tuska ,po i $card z $whatToPrint")
            throw e
        }
    }

    private var lastUUID = 3000L
    fun getNextUUID(): Long {
        return lastUUID++
    }

    private var lastVirtualUUID = -1000L
    fun getUnknowCard(): Card {
        return Card(UnknowCard, lastVirtualUUID--, null)
    }



}
