package walic.pureEngine

class RadnomAdapter(private val quiete: Boolean, val random: kotlin.random.Random) : PlayerAdapter() {
    lateinit var state: State
    lateinit var sih: SecretInformationHandler
    var playerId: Int = -1000
    val log = mutableListOf<String>()


    override fun setRequierd(state: State, noDeck: Boolean): Pair<List<String>, Long> {


        this.state = state
        this.sih = state.steamAdapter.secretInformationHandler
        this.playerId = state.steamAdapter.playerNumber

        return Pair(
                when {
                    noDeck -> listOf()
                    else -> {
                        val leader = allLeaders.random(random)
                        val fraction = leader.fraction!!
                        val possible = allNormalCards.filter { it.isFraction(fraction) }
                        val deck = mutableListOf(leader)
                        possible.filter { it.ist(Tag.Gold) }.pickUpToN(4, random).forEach { deck.add(it) }
                        possible.filter { it.ist(Tag.Silver) }.pickUpToN(6, random).forEach { deck.add(it) }
                        possible.filter { it.ist(Tag.Bronze) }.pickUpToN(5, random).forEach { card -> repeat(3) { deck.add(card) } }
                        deck.map { it::class.simpleName!! }
                    }
                }, random.nextLong())
    }

    override suspend fun printMess(printOption: State.PrintOption, card: Card?) {
        if (quiete)
            log.add("$printOption by $card")
        else if (printOption !is State.PrintOption.InfoForPlayer)
            println("$printOption by $card")
    }

    override suspend fun pickRow(mask: RowMask): CardLocation {
        val posibilites = mutableListOf<CardLocation>()
        mask.ally.forEachIndexed { index, b -> if (b) posibilites.add(CardLocation(playerId, index)) }
        mask.enemy.forEachIndexed { index, b -> if (b) posibilites.add(CardLocation(1 - playerId, index)) }
        return posibilites.random(random)
    }

    override suspend fun pickPlaceBetweenCards(card: Card, mask: RowMask): CardLocation {
        val rowLocation = pickRow(mask)
        rowLocation.positionId = random.nextInt(0, state.getRowByLocation(rowLocation).size + 1)
        return rowLocation
    }


    override suspend fun <A> pickFromList(list: List<A>, n: Int, canLess: Boolean): List<Int> {
        val howMany = if (canLess) {
            if (n == 1)
                if (random.nextFloat() < 0.9) 1 else 0
            else
                random.nextInt(0, n + 1)
        } else
            n
        return list.indices.toMutableList().removeUpToN(howMany, random)
    }

    override fun nextRandom(): Long {
       return random.nextLong()
    }
}

