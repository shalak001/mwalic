package walic.pureEngine

import kotlinx.serialization.Serializable

@Serializable
data class CardLocation(var playerId: Int, var rowId: Int, var positionId: Int = 0) {
    fun left(): CardLocation {
        return CardLocation(playerId, rowId, positionId - 1)
    }

    fun right(): CardLocation {
        return CardLocation(playerId, rowId, positionId + 1)
    }

    fun opositeRow(): CardLocation {
        return CardLocation(1 - playerId, rowId, 0)
    }

    override fun toString(): String {
        return "$playerId $rowId $positionId"
    }

}

fun Card.owner(): Int {
    val p = state!!.players.mapIndexed { index, player -> if ((player.graveyard + player.rows.flatten() + player.banished + player.knownHand + player.spawners).contains(this)) index else null }.filterNotNull().firstOrNull() ?: throw IllegalArgumentException(this.toString())
    return if (ist(Tag.SPY))
        1 - p
    else
        p
}

fun Card.location(): CardLocation {
    val state = state!!
    for (p in 0..1)
        for (l in 0..2) {
            val i = state.players[p].rows[l].indexOf(this)
            if (i != -1)
                return CardLocation(p, l, i)
        }
    throw  WrongInputException(toString())
}

fun Card.getNAdjacent(ammount: Int): List<Card> {
    val location = location()
    val lane = state!!.getRowByLocation(location)
    return (-ammount..ammount).mapNotNull { lane.getOrNull(it + location.positionId) }
}
