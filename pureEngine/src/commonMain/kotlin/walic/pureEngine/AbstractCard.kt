package walic.pureEngine

import kotlinx.serialization.KSerializer
import kotlinx.serialization.Serializable
import kotlinx.serialization.descriptors.PrimitiveKind
import kotlinx.serialization.descriptors.PrimitiveSerialDescriptor
import kotlinx.serialization.descriptors.SerialDescriptor
import kotlinx.serialization.encoding.Decoder
import kotlinx.serialization.encoding.Encoder


object CardAsStringSerializer : KSerializer<AbstractCard> {
    override val descriptor: SerialDescriptor = PrimitiveSerialDescriptor("Card", PrimitiveKind.STRING)

    override fun deserialize(decoder: Decoder): AbstractCard {
        return getCard(decoder.decodeString())
    }

    override fun serialize(encoder: Encoder, value: AbstractCard) {
        encoder.encodeString(value.name)
    }
}

@Serializable(with = CardAsStringSerializer::class)
abstract class AbstractCard {
    fun ist(tag: Tag): Boolean {
        return tags.contains(tag)
    }

    override fun toString(): String {
        return getNiceName()
    }


    val fraction: Tag?
        get() = tags.filter { fractions.contains(it) }.elementAtOrNull(0)
    fun isFraction(tag: Tag): Boolean {
        return fraction == null || fraction == tag
    }


    fun getNiceName(): String {
        return name.split(".").last().replace(Regex("([A-Z])"), " $1")
    }

    fun getMaxInInnitialDeck(): Int {
        if (ist(Tag.Gold))
            return 1
        if (ist(Tag.Silver))
            return 1
        if (ist(Tag.Bronze))
            return 3
        if (ist(Tag.Leader))
            return 1
        throw IllegalArgumentException(name)
    }

    fun asFanyText(): List<String> {
        val ofTheYedi = mutableListOf<String>()
        val betterName = getNiceName()
        ofTheYedi.add(betterName)
        var accu = "Tags: "
        for (t in tags) {
            accu += "$t, "
            if (accu.length > 50) {
                ofTheYedi.add(accu)
                accu = "   "
            }
        }
        ofTheYedi.add(accu)
        ofTheYedi.add("Description:")
        for (d in description.split("\n")) {
            var lol = ""
            for (dd in d.split(" ")) {
                lol += " $dd"
                if (lol.length > 50) {
                    ofTheYedi.add(lol)
                    lol = "     "
                }
            }
            ofTheYedi.add("$lol;")
        }
        if (!ist(Tag.SPECIAL))
            ofTheYedi.add("Strenght: $defaultStrenght")
        return ofTheYedi
    }

    enum class PlayMod {
        FromHand,
        FromHandReveled,
        FromDeck,
        Resurected,
        Called,
        Spawned,
    }

    data class AuxCallData(val crewCount: Int, val playMod: PlayMod)


    abstract val defaultStrenght: Int
    abstract val tags: List<Tag>
    abstract val description: String

    val name = this::class.simpleName!!


    open suspend fun onEntry(state: State, playerId: Int, auxData: AuxCallData, thisCard: Card) {}
    open suspend fun onTurnStart(state: State, playerId: Int, thisCard: Card) {}
    open suspend fun onTurnStartInGraviard(state: State, playerId: Int, thisCard: Card) {}
    open suspend fun onTurnsEnd(state: State, playerId: Int, thisCard: Card) {}
    open suspend fun onTurnsEndInGraviard(state: State, playerId: Int, thisCard: Card) {}
    open suspend fun onStartOfRoundInGraviard(state: State, playerId: Int, thisCard: Card) {}

    open fun onStartOfGame(sih: SecretInformationHandler) {}

    //multi card
    open suspend fun onCardAppear(state: State, card: Card, thisCard: Card, playMod: PlayMod) {}
    open suspend fun onCardAppearInGraviard(state: State, playerId: Int, card: Card, cardPlayerId: Int, playMod: PlayMod, thisCard: Card) {}
    open suspend fun onCardDamaged(state: State, card: Card, thisCard: Card) {}
    open suspend fun onCardDiscared(state: State, card: Card, thisCard: Card) {}
    open suspend fun onCardReveled(state: State, card: Card, thisCard: Card, revelerId: Int) {}

    //one card
    open suspend fun onDeath(state: State, thisCard: Card) {}
    open suspend fun onArmomrLost(state: State, thisCard: Card) {}
    open suspend fun onDamaged(state: State, thisCard: Card) {}
    open suspend fun onMoved(state: State, thisCard: Card) {}
    open suspend fun onRevelad(state: State, thisCard: Card) {}
    open suspend fun onDiscarded(state: State, thisCard: Card) {}
    open suspend fun onPutInGraviard(state: State, thisCard: Card) {}


}