package walic.pureEngine

import kotlinx.serialization.Serializable
import kotlinx.serialization.Transient
import kotlin.random.Random

enum class Tag {
    Aedirn,
    Agent,
    Alchemy,
    Beast,
    Bronze,
    Cintra,
    Construct,
    Crewd,
    Cultist,
    Cursed,
    Dimun,
    Doomed,
    Draconid,
    Drummond,
    Dryad,
    Dwarf,
    Elf,
    Gold,
    Hazard,
    Heymaey,
    Item,
    Kaedwen,
    Leader,
    Machine,
    Mage,
    Nilfgaard,
    NorthenKingdom,
    Officer,
    Ogroid,
    Organic,
    Redania,
    Relict,
    SINGLEUSE,
    SPECIAL,
    SPY,
    Silver,
    Skelige,
    Skojatel,
    Soldier,
    Spell,
    Support,
    Tactic,
    Temeria,
    TOKEN,
    Tuirseach,
    Vampire,
    Witcher,
    Insectoid,
    Vodyanoi,
    Monsters,
    Fleeting,
    Necrophage,
    Siren,
    AnCraite,
    Brokvar,
    IMMUNNE,
    WildHunt,
    SPAWNER,
    Ambush,
}


@Serializable
class Card(
    var ac: AbstractCard,
    val uuid: Long,
    @Transient var state: State? = null,
    var baseStrength: Int,
    var currentStrength: Int,
    var locked: Boolean,
    var resilience: Boolean,
    var armour: Int,
    var didUseAbility: Boolean,
    var counter: Int,
    val additionlTags: MutableList<Tag>
) {


    constructor(ac: AbstractCard, uuid: Long, state: State?) : this(ac, uuid, state, ac.defaultStrenght, ac.defaultStrenght, false, false, 0, false, 0, mutableListOf())

    suspend fun onEntry(auxData: AbstractCard.AuxCallData) = ac.onEntry(state!!, owner, auxData, this)

    suspend fun onTurnStart() = ac.onTurnStart(state!!, owner, this)
    suspend fun onTurnStartInGraviard() = ac.onTurnStartInGraviard(state!!, owner, this)
    suspend fun onTurnsEnd() = ac.onTurnsEnd(state!!, owner, this)
    suspend fun onTurnsEndInGraviard() = ac.onTurnsEndInGraviard(state!!, owner, this)
    suspend fun onStartOfRoundInGraviard() = ac.onStartOfRoundInGraviard(state!!, owner, this)

    fun onStartOfGame(sih: SecretInformationHandler) = ac.onStartOfGame(sih)

    //multi card
    suspend fun onCardAppear(card: Card, playMod: AbstractCard.PlayMod) = ac.onCardAppear(state!!, card, this, playMod)
    suspend fun onCardAppearInGraviard(card: Card, playMod: AbstractCard.PlayMod) = ac.onCardAppearInGraviard(state!!, owner, card, card.owner, playMod, this)
    suspend fun onCardDamaged(card: Card) = ac.onCardDamaged(state!!, card, this)
    suspend fun onCardDiscared(card: Card) = ac.onCardDiscared(state!!, card, this)
    suspend fun onCardReveled(card: Card, revelerId: Int) = ac.onCardReveled(state!!, card, this, revelerId)

    //one card
    suspend fun onDeath() = ac.onDeath(state!!, this)
    suspend fun onArmomrLost() = ac.onArmomrLost(state!!, this)
    suspend fun onDamaged() = ac.onDamaged(state!!, this)
    suspend fun onMoved() = ac.onMoved(state!!, this)
    suspend fun onRevelad() = ac.onRevelad(state!!, this)
    suspend fun onDiscarded() = ac.onDiscarded(state!!, this)
    suspend fun onPutInGraviard() = ac.onPutInGraviard(state!!, this)


    override fun toString(): String {
        if (ac is UnknowCard)
            return "Unknow Card"
        var ofTheYedi = ac.getNiceName()
        if (!ist(Tag.SPECIAL)) {
            if (armour != 0)
                ofTheYedi += " A:$armour"
            ofTheYedi += if (currentStrength != baseStrength) " $currentStrength/$baseStrength" else " $baseStrength"

            if (locked)
                ofTheYedi += "X"
        } else
            ofTheYedi += " Spell"
        if (counter > 0)
            ofTheYedi += " C$counter"
        if (ist(Tag.SINGLEUSE) && didUseAbility)
            ofTheYedi += "U"
        additionlTags.forEach { ofTheYedi += " +${it}" }
        return ofTheYedi
    }

    fun isFraction(tag: Tag): Boolean {
        return fraction == null || fraction == tag
    }

    val tags: List<Tag>
        get() = ac.tags

    val name: String
        get() = ac.name

    val fraction: Tag?
        get() = tags.filter { fractions.contains(it) }.elementAtOrNull(0)

    val owner: Int
        get() = owner()

    fun defaultCopy(long: Long): Card {
        return Card(this.ac, long, state)
    }


    fun ist(tag: Tag): Boolean {
        return tags.contains(tag) != additionlTags.contains(tag)
    }

    fun isDamaged(): Boolean {
        return baseStrength > currentStrength
    }

    fun color(): Tag {
        return listOf(Tag.Gold, Tag.Silver, Tag.Bronze, Tag.Leader, Tag.TOKEN).firstOrNull { ist(it) }
            ?: throw IllegalArgumentException(ac.name)
    }
}


object UnknowCard : AbstractCard() {
    override val defaultStrenght = -1
    override val tags = listOf<Tag>()
    override val description: String = " testCard Pls ignore"
    override fun toString(): String {
        return "Unknow Card"
    }
}


fun Card.asFanyText(): List<String> {
    val ofTheYedi = mutableListOf<String>()
    val betterName = ac.getNiceName()
    ofTheYedi.add(betterName)
    var accu = "Tags: "
    for (t in tags) {
        accu += "$t, "
        if (accu.length > 50) {
            ofTheYedi.add(accu)
            accu = "   "
        }
    }
    ofTheYedi.add(accu)
    ofTheYedi.add("Description:")
    for (d in ac.description.split("\n")) {
        var lol = ""
        for (dd in d.split(" ")) {
            lol += " $dd"
            if (lol.length > 50) {
                ofTheYedi.add(lol)
                lol = "     "
            }
        }
        ofTheYedi.add("$lol;")
    }
    val s = if (baseStrength != currentStrength)
        "$currentStrength/$baseStrength"
    else
        "$currentStrength"
    if (!ist(Tag.SPECIAL))
        ofTheYedi.add("Strenght: $s")
    if (locked)
        ofTheYedi.add("Locked")
    if (armour != 0)
        ofTheYedi.add("Armour $armour")
    if (didUseAbility && ist(Tag.SINGLEUSE))
        ofTheYedi.add("Abilit alredy used!!!")
    return ofTheYedi
}


abstract class Weather {
    abstract suspend fun onTurn(state: State, row: List<Card>, random: Random)
}

val fractions = listOf(Tag.NorthenKingdom, Tag.Nilfgaard, Tag.Skelige, Tag.Skojatel)

