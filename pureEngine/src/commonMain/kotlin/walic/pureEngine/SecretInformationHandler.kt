package walic.pureEngine




class SecretInformationHandler(val playerId: Int) {
    //

    val cardToDraw :MutableList<Card>
    get() {
        cardAtTop.removeAll { !cardToDrawInternal.contains(it) } // brzydkie ale cotam
        cardAtBottom.removeAll { !cardToDrawInternal.contains(it) }
        return cardToDrawInternal
    }
    private val cardToDrawInternal = mutableListOf<Card>()
    val cardInHand = mutableListOf<Card>()
    val cardAtBottom = mutableListOf<Card>()
    val cardAtTop = mutableListOf<Card>()

    var initialDeck: List<AbstractCard>? = null
    var fraction: Tag? = null


    fun getNTopCard(amount: Int, filter: (Card) -> Boolean = { true }): List<Card> {

        val ofTheYedi = mutableListOf<Card>()
        repeat(amount) {
            var card: Card?
            card = cardAtTop.filter(filter).elementAtOrNull(0)
            if (card == null) {
                card = cardToDraw.filter { !cardAtTop.contains(it) }.filter(filter).randomOrNull(random)
                if (card == null) {
                    card = cardAtBottom.lastOrNull()
                }
            }

            if (card == null)
                return ofTheYedi
            cardAtBottom.remove(card)
            cardToDraw.remove(card)
            cardAtTop.remove(card)

            ofTheYedi.add(card)
        }
        if (ofTheYedi.distinct().size != ofTheYedi.size)
            throw WrongInputException("Podwójna w do dobranie $ofTheYedi")
        return ofTheYedi
    }

    var random = XorWowRandomCustom(0L)


    fun setRandom(long: Long) {
        random = XorWowRandomCustom(secretSeed + long)
    }


    var secretSeed = 0L

    fun moveToTop(card: Card) {
        if (!cardToDraw.contains(card))
            return
        cardAtTop.remove(card)
        cardAtBottom.remove(card)
        cardAtTop.add(0, card)
    }

    fun moveToBottom(card: Card) {
        if (!cardToDraw.contains(card))
            return
        cardAtTop.remove(card)
        cardAtBottom.remove(card)
        cardAtBottom.add(0, card)
    }

    var uuid = 1000L * playerId + 100L
    fun loadCards(data: Pair<List<String>, Long>): Card {
        secretSeed = data.second
        val pair = praseDeck(data.first) { uuid++ }
        val leader = pair.first

        cardToDraw.addAll(pair.second)
        checkIfDeckOk(leader.ac, cardToDraw.map { it.ac })
        fraction = leader.fraction
        var i = 0
        while (i < cardToDraw.count()) {
            cardToDraw[i].onStartOfGame(this)
            i++
        }
        cardToDraw.shuffle(this.random)
        initialDeck = cardToDraw.map { it.ac }
        return leader
    }


}

fun praseDeck(split: List<String>, f: () -> Long): Pair<Card, List<Card>> {
    val splitNames = split.map { it.trim() }.filter { it.isNotEmpty()&& !it.startsWith("#") }
    val leader = Card(allLeaders.find { it.name == splitNames[0] }?:throw WrongInputException(" |${splitNames[0]}|"), f(), null)
    val cardToDraw = splitNames.drop(1).map {  name -> Card(allNormalCards.find { it.name == name }?:throw WrongInputException("|$name|"), f(), null) }
    return Pair(leader, cardToDraw)
}

const val maxGold = 4
const val maxSilver = 6
fun checkIfDeckOk(leader: AbstractCard, cardToDraw: List<AbstractCard>) {
    if (!leader.ist(Tag.Leader)) {
        throw  WrongInputException("Leader $leader is not a leader.")
    }
    val fraction = leader.fraction

    cardToDraw.groupBy { it.name }.forEach { (name, list) ->
        if (list.size > list[0].getMaxInInnitialDeck())
            throw WrongInputException("To many copies of $name (${list.size}/${list[0].getMaxInInnitialDeck()}")
        if (list[0].fraction != null && list[0].fraction != fraction)
            throw  WrongInputException("Card $name fraction is ${list[0].fraction}  while the leader fraction is $fraction")
        if (list[0].ist(Tag.Leader))
            throw WrongInputException("Second Leader - $name")
    }
    if (cardToDraw.filter { it.ist(Tag.Gold) }.size > maxGold)
        throw WrongInputException("Too many gold cards")
    if (cardToDraw.filter { it.ist(Tag.Silver) }.size > maxSilver)
        throw WrongInputException("Too many silver cards")
    if (cardToDraw.size < 25 || cardToDraw.size > 40)
        throw  WrongInputException("Wrong card number (25 => ${cardToDraw.size}<=40 )")
}
